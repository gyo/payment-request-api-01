# Payment Request API

練習用リポジトリ

## 参考

- https://developer.mozilla.org/en-US/docs/Web/API/PaymentRequest/PaymentRequest
- https://developers.google.com/web/fundamentals/payments/

## プロセス

- サイトがブラウザに `PaymentRequest` を渡す
- ブラウザがユーザーに 支払い UI を表示する
- ユーザーがサイトにカード情報などを送信する（サイト？）

利点

- ユーザーは、リクエスト、承認、支払、結果、という操作がすべて 1 つのステップで実行できる
- ウェブサイトは JavaScript API 呼び出しが 1 つのみになる

## Payment Request API の基本

### PaymentRequest

```
const request = new PaymentRequest(
  methodData: {
    supportedMethods,
    data
  },
  details: {
    id,
    total,
    displayItems,
    shippingOptions,
    modifiers: {
      additionalDisplayItems,
      data,
      total
    }
  },
  options: {
    requestPayerName,
    requestPayerEmail,
    requestPayerPhone,
    requestShipping,
    shippingType
  }
);

request.show().then(() => {});

setTimeout(() => {
  request.abort().then(() => {})
}, 9999)
```

#### methodData

`supportedMethods` は、`"basic-card"` など。

`data` の構造は [BasicCardRequest](https://developer.mozilla.org/en-US/docs/Web/API/BasicCardRequest) 辞書で定義されている。

#### details

`total` は、請求される合計額。

`displayItems` は、最終金額の計算方法。商品ではなく、小計、割引額、税、送料といった、金額にかかわるデータのリスト。

`total` と、`displayItems` の合計金額が一致することは保証されない。

#### show()

PaymentRequest インターフェースをアクティブにする。

`Promise` が返る。

#### abort()

PaymentRequest を中止する。

取引中に売り切れになった場合などに使う。

`Promise` が返る。

### PaymentResponse

`show()` メソッドの Promise が解決されると、[PaymentResponse](https://developer.mozilla.org/en-US/docs/Web/API/PaymentResponse) が入手できる。

このとき、UI にはスピナーが表示される。

PaymentResponse を受け取ったら、アプリは決済サービスに情報を送信する。

決済処理が終わったら、`complete()` メソッドを呼び出して UI を閉じる。

プロパティは全て ReadOnly 。

```
{
  details,
  methodName,
  payerEmail,
  payerName,
  payerPhone,
  requestId,
  shippingAddress,
  shippingOption
}
```

#### complete()

Promise が返る。

引数は Optional で、`"success"`, `"fail"`, `"unknown"` がとれる。
