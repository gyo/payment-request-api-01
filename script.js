import paymentMethodData from "./payment-parameters/paymentMethodData.js";
import paymentDetails from "./payment-parameters/paymentDetails.js";

const payAsync = async paymentRequest => {
  const paymentResponse = await paymentRequest.show();

  console.log(paymentResponse);

  const responseFromPaymentService = await fetch("pay.json");

  console.log(responseFromPaymentService);

  if (responseFromPaymentService.status !== 200) {
    paymentResponse.complete("fail");
    return;
  }

  const jsonFromPaymentService = await responseFromPaymentService.json();

  console.log(jsonFromPaymentService);

  if (jsonFromPaymentService.message === "LGTM") {
    paymentResponse.complete("success");
  } else {
    paymentResponse.complete("fail");
  }
};

try {
  const button = document.querySelector("#button");

  button.addEventListener("click", e => {
    e.preventDefault();

    const paymentRequest = new PaymentRequest(paymentMethodData, paymentDetails);

    console.log(paymentRequest);

    payAsync(paymentRequest);

    setTimeout(() => {
      return paymentRequest.abort();
    }, 1000 * 60 * 2);
  });
} catch (error) {
  console.error(error);
}
